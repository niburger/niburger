import sys
sys.path.insert(0, '../flask')

from adminmanager import AdminManager
from helper import table2dict

def process(engine, wb):
    burger = wb['burger_contains']
    res = table2dict(318, 2, burger)

    adm = AdminManager(engine)
    for r in res:
        adm.add_ingr_to_burger(r['id_burger'], r['id_ingr'])
