def row2dict(rownum, colcnt, sheet):
    row = str(rownum)
    ret = {}
    for i in range(0, colcnt):
        col = chr(ord('A') + i)
        colname = sheet[col + '1'].value
        ret[colname] = sheet[col + row].value

    return ret

def table2dict(maxrow, colcnt, sheet):
    ret = []
    for i in range(2, maxrow+1):
        ret.append(row2dict(i, colcnt, sheet))
    return ret
