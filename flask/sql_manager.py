from sqlalchemy import create_engine

class SQLManager:
    def __init__(self, engine_url):
        self.engine = create_engine(engine_url)
    def execute(self, query):
        return self.engine.execute(query)
    def fetchall(self, query):
        return self.engine.execute(query).fetchall()
