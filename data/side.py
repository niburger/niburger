import sys
sys.path.insert(0, '../flask')

from adminmanager import AdminManager
from helper import table2dict

def process(engine, wb):
    side = wb['side']
    res = table2dict(178, 7, side)

    engine.execute("alter table Side auto_increment = 1")
    adm = AdminManager(engine)
    for r in res:
        adm.add_side(r)
