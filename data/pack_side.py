import sys
sys.path.insert(0, '../flask')

from adminmanager import AdminManager
from helper import table2dict

def process(engine, wb):
    ps = wb['pack_side']
    res = table2dict(25, 3, ps)

    adm = AdminManager(engine)
    for r in res:
        adm.add_side_into_pack(r['id_pack'], r['id_side'], r['cardinality'])
