import sys
sys.path.insert(0, '..')

from sqlalchemy import create_engine
from dbmanager import DBManager
from adminmanager import AdminManager

engine = create_engine("mysql://root:ab1616@localhost/test")

dbm = DBManager(engine)
adm = AdminManager(engine)

engine.execute("delete from Ingredient")

ingr = {
        'type': 'bread',
        'name': 'bread',
        'dsc': 'hi',
        'img': 'bread.png'
        }

adm.add_ingr(ingr)
target = adm.search_ingr('b', 'r')[0]['id_ingr']
ingr.update({'id_ingr': target})
ingr['dsc'] = 'hello'
adm.edit_ingr(ingr)

print adm.get_ingrs()
adm.add_ingr(ingr)
adm.delete_ingr(target)
print adm.get_ingrs()

ingr['name'] = 'chick'
adm.add_ingr(ingr)
ingr['name'] = 'tomato'
adm.add_ingr(ingr)

print adm.get_ingrs()

print "Test of Gp 2 Done!"
