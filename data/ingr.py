import sys
sys.path.insert(0, '../flask')

from adminmanager import AdminManager
from helper import table2dict

def process(engine, wb):
    ingr = wb['ingredient']
    res = table2dict(29, 5, ingr)

    engine.execute("alter table Ingredient auto_increment = 1")
    adm = AdminManager(engine)
    for r in res:
        adm.add_ingr(r)
