import sys
sys.path.insert(0, '../flask')

from adminmanager import AdminManager
from helper import table2dict

def process(engine, wb):
    pack = wb['pack']
    res = table2dict(10, 6, pack)

    engine.execute("alter table Pack auto_increment = 1")
    adm = AdminManager(engine)
    for r in res:
        r['burgers'] = []
        r['sides'] = []
        adm.add_pack(r)
