# -*- coding: utf-8 -*-

from sqlalchemy import text
from copy import deepcopy

class AdminManager:
    def __init__(self, engine):
        self.engine = engine

    def res2dic(self, resultProxy):
        rows = resultProxy.fetchall()
        return map(lambda r: dict(zip(r.keys(), r.values())), rows)

    def cover(self, str):
        return '%'+str+'%'


    # Group 1
    def get_brands(self):
        cmd = "select * from Brand"
        return self.res2dic(self.engine.execute(text(cmd)))

    def add_brand(self, query):
        cmd = "insert into Brand (name, contact, homepage, img) " \
                "values (:name, :contact, :homepage, :img)"
        return bool(self.engine.execute(text(cmd), query).rowcount)

    def search_brand(self, name):
        cmd = "select * from Brand " \
                "where name like :name"
        return self.res2dic(self.engine.execute(text(cmd), {'name': self.cover(name)}))

    def edit_brand(self, query):
        cmd = "update Brand " \
                "set name=:name, contact=:contact, homepage=:homepage, img=:img " \
                "where id_brand = :id_brand"
        return bool(self.engine.execute(text(cmd), query).rowcount)

    def delete_brand(self, brand):
        cmd = "delete from Brand " \
                "where id_brand = :brand"
        return bool(self.engine.execute(text(cmd), {'brand': brand}).rowcount)


    # Group 2
    def get_ingrs(self):
        cmd = "select * from Ingredient"
        return self.res2dic(self.engine.execute(text(cmd)))

    def add_ingr(self, query):
        cmd = "insert into Ingredient (type, name, dsc, img) " \
                "values (:type, :name, :dsc, :img)"
        return bool(self.engine.execute(text(cmd), query).rowcount)

    def search_ingr(self, type, name):
        cmd = "select * from Ingredient " \
                "where type like :type " \
                "and name like :name"
        return self.res2dic(self.engine.execute(text(cmd), {'type': self.cover(type), 'name': self.cover(name)}))

    def edit_ingr(self, query):
        cmd = "update Ingredient " \
                "set type=:type, name=:name, dsc=:dsc, img=:img " \
                "where id_ingr = :id_ingr"
        return bool(self.engine.execute(text(cmd), query).rowcount)

    def delete_ingr(self, ingr):
        cmd = "delete from Ingredient " \
                "where id_ingr = :ingr"
        return bool(self.engine.execute(text(cmd), {'ingr': ingr}).rowcount)


    # Group 3
    def add_burger(self, query):
        cmd = "insert into Burger " \
                "(ea_name, ea_dsc, ea_price, ea_img, " \
                "set_name, set_dsc, set_price, set_img, " \
                "has_lunch, lunch_price, id_brand) " \
                "values (:ea_name, :ea_dsc, :ea_price, :ea_img, " \
                ":set_name, :set_dsc, :set_price, :set_img, " \
                ":has_lunch, :lunch_price, :id_brand)"
        if not self.engine.execute(text(cmd), query).rowcount:
            return False

        cmd = "select last_insert_id() as burger"
        burger = self.res2dic(self.engine.execute(text(cmd)))[0]['burger']

        cmd = "insert into Burger_contains (id_burger, id_ingr) " \
                "values (:burger, :ingr)"

        for ingr in query['ingredients']:
            if not self.engine.execute(text(cmd), {'burger': burger, 'ingr': ingr}).rowcount:
                return False

        return True

    def edit_burger(self, query):
        cmd = "update Burger " \
                "set ea_name=:ea_name, ea_dsc=:ea_dsc, ea_price=:ea_price, ea_img=:ea_img, " \
                "set_name=:set_name, set_dsc=:set_dsc, set_price=:set_price, set_img=:set_img, " \
                "has_lunch=:has_lunch, lunch_price=:lunch_price " \
                "where id_burger = :id_burger"
        return bool(self.engine.execute(text(cmd), query).rowcount)

    def add_ingr_to_burger(self, burger, ingr):
        cmd = "insert into Burger_contains (id_burger, id_ingr) " \
                "values (:burger, :ingr)"
        return bool(self.engine.execute(text(cmd), {'burger': burger, 'ingr': ingr}).rowcount)

    def delete_ingr_from_burger(self, burger, ingr):
        cmd = "delete from Burger_contains " \
                "where id_burger=:burger and id_ingr=:ingr"
        return bool(self.engine.execute(text(cmd), {'burger': burger, 'ingr': ingr}).rowcount)

    def search_burger(self, burger_name, brand_name):
        cmd = "select id_brand from Brand " \
                "where name like :brand_name"
        brand_ids = self.res2dic(self.engine.execute(text(cmd), {'brand_name': self.cover(brand_name)}))

        cmd = "select * from Burger " \
                "where ea_name like :ea_name " \
                "and id_brand = :id_brand"
        return sum(
                map(lambda brand: self.res2dic(
                    self.engine.execute(text(cmd), {'id_brand': brand['id_brand'], 'ea_name': self.cover(burger_name)})
                    ), brand_ids),
                [])

    def get_ingrs_from_burger(self, burger):
        cmd = "select id_ingr from Burger_contains " \
                "where id_burger = :burger"
        return self.res2dic(self.engine.execute(text(cmd), {'burger': burger}))


    # Group 4
    def add_side(self, query):
        cmd = "insert into Side (type, name, dsc, price, img, id_brand) " \
                "values (:type, :name, :dsc, :price, :img, :id_brand)"
        return bool(self.engine.execute(text(cmd), query).rowcount)

    def edit_side(self, query):
        cmd = "update Side " \
                "set type=:type, name=:name, dsc=:dsc, price=:price, img=:img " \
                "where id_side = :id_side"
        return bool(self.engine.execute(text(cmd), query).rowcount)

    def search_side(self, side_name, brand_name):
        cmd = "select id_brand from Brand " \
                "where name like :brand_name"
        brand_ids = self.res2dic(self.engine.execute(text(cmd), {'brand_name': self.cover(brand_name)}))

        cmd = "select * from Side " \
                "where name like :name " \
                "and id_brand = :id_brand"

        return sum(
                map(lambda brand: self.res2dic(
                    self.engine.execute(text(cmd), {'id_brand': brand['id_brand'], 'name': self.cover(side_name)})
                    ), brand_ids),
                [])

    def delete_side(self, side):
        cmd = "delete from Side " \
                "where id_side = :side"
        return bool(self.engine.execute(text(cmd), {'side': side}))


    # Group 5
    def add_pack(self, query):
        cmd = "insert into Pack (name, price, dsc, img, id_brand) " \
                "values (:name, :price, :dsc, :img, :id_brand)"
        if not self.engine.execute(text(cmd), query).rowcount:
            return False

        cmd = "select last_insert_id() as pack"
        pack = self.res2dic(self.engine.execute(text(cmd)))[0]['pack']

        cmd = "insert into Pack_burger (id_pack, id_burger, cardinality) " \
                "values (:pack, :id_burger, :cardinality)"

        for burger in query['burgers']:
            if not self.engine.execute(text(cmd),
                    {'pack': pack, 'id_burger': burger['id_burger'], 'cardinality': burger['cardinality']}
                    ).rowcount:
                return False

        cmd = "insert into Pack_side (id_pack, id_side, cardinality) " \
                "values (:pack, :id_side, :cardinality)"

        for side in query['sides']:
            if not self.engine.execute(text(cmd),
                    {'pack': pack, 'id_side': side['id_side'], 'cardinality': side['cardinality']}
                    ).rowcount:
                return False

        return True

    def edit_pack(self, query):
        cmd = "update Pack " \
                "set name=:name, price=:price, dsc=:dsc, img=:img " \
                "where id_pack = :id_pack"
        return bool(self.engine.execute(text(cmd), query).rowcount)

    def add_burger_into_pack(self, pack, burger, card):
        cmd = "insert into Pack_burger (id_pack, id_burger, cardinality) " \
                "values (:pack, :burger, :card)"
        return bool(self.engine.execute(text(cmd), {'pack': pack, 'burger': burger, 'card': card}).rowcount)

    def delete_burger_from_pack(self, pack, burger):
        cmd = "delete from Pack_burger " \
                "where id_pack = :pack and id_burger = :burger"
        return bool(self.engine.execute(text(cmd), {'pack': pack, 'burger': burger}).rowcount)

    def add_side_into_pack(self, pack, side, card):
        cmd = "insert into Pack_side (id_pack, id_side, cardinality) " \
                "values (:pack, :side, :card)"
        return bool(self.engine.execute(text(cmd), {'pack': pack, 'side': side, 'card': card}).rowcount)

    def delete_side_from_pack(self, pack, side):
        cmd = "delete from Pack_side " \
                "where id_pack = :pack and id_side = :side"
        return bool(self.engine.execute(text(cmd), {'pack': pack, 'side': side}).rowcount)

    def search_pack(self, name):
        cmd = "select * from Pack " \
                "where name like :name"
        return self.res2dic(self.engine.execute(text(cmd), {'name': self.cover(name)}))

    def delete_pack(self, pack):
        cmd = "delete from Pack " \
                "where id_pack = :pack"
        return bool(self.engine.execute(text(cmd), {'pack': pack}).rowcount)


    # Group 6
    def add_upgrade(self, query):
        cmd = "insert into Upgrade (name, dsc, price, img, id_brand, id_side) " \
                "values (:name, :dsc, :price, :img, :id_brand, :id_side)"
        return bool(self.engine.execute(text(cmd), query).rowcount)

    def edit_upgrade(self, query):
        cmd = "update Upgrade " \
                "set name=:name, dsc=:dsc, price=:price, img=:img "\
                "where id_upgrade = :id_upgrade"
        return bool(self.engine.execute(text(cmd), query).rowcount)

    def search_upgrade(self, name):
        cmd = "select * from Upgrade " \
                "where name like :name"
        return self.res2dic(self.engine.execute(text(cmd), {'name': self.cover(name)}))

    def delete_upgrade(self, upgrade):
        cmd = "delete from Upgrade " \
                "where id_upgrade = :upgrade"
        return bool(self.engine.execute(text(cmd), {'upgrade': upgrade}))
