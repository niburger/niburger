from flask import Flask, render_template, request, jsonify
from sqlalchemy import create_engine
from dbmanager import DBManager

app = Flask(__name__)
engine = create_engine("mysql://root:119mysql!@localhost/niburger_test")
dbm = DBManager(engine)

# Group 1
@app.route("/init/get_brand")
def init_get_brand():
    ret = {"results": dbm.getBrandNames()}
    return jsonify(**ret)

@app.route("/init/get_ingr")
def init_get_ingr():
    ret = {"results": dbm.getIngrs()}
    return jsonify(**ret)

@app.route("/init/get_upgr", methods=['POST'])
def init_get_upgr():
    req = request.get_json()
    ids_brand = req["ids_brand"]
    ret = {"results": dbm.getUpgrades(ids_brand)}
    return jsonify(**ret)

# Group 2
@app.route("/panel/basic", methods=['POST'])
def panel_basic():
    req = request.get_json()
    ret = dbm.getMenus(req["ids_brand"], req["seed"])
    return jsonify(**ret)

@app.route("/panel/my_burger", methods=['POST'])
def panel_my_burger():
    req = request.get_json()
    ret = {"results", dbm.getMenusByIngrs(req["ids_brand"], req["ids_ingr"], req["seed"])}
    return jsonify(**ret)

# Group 3
@app.route("/details/burger/<id_burger>")
def details_burger(id_burger):
    ret = dbm.getBurgerInfo(id_burger)
    return jsonify(**ret)

@app.route("/details/pack/<id_pack>")
def details_pack(id_pack):
    ret = dbm.getPackInfo(id_pack)
    return jsonify(**ret)

# Group 4
@app.route("/car/smart_set", methods=['POST'])
def cart_smart_set():
    req = request.get_json()
    ret = {"results": dbm.smartSet(req["ids_burger"], req["ids_side"])}
    return jsonify(**ret)



if __name__ == "__main__":
    app.run(host='192.168.0.6', port=8000, debug=True)
