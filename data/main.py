from openpyxl import load_workbook
from sqlalchemy import create_engine

import brand, side, burger, upgrade, ingr, pack, pack_burger, pack_side, burger_contains

wb = load_workbook('niburger.xlsx', guess_types = True)
engine = create_engine('mysql://root:119mysql!@localhost/niburger?charset=utf8')

brand.process(engine, wb)
print "brand"

side.process(engine, wb)
print "side"

burger.process(engine, wb)
print "burger"

upgrade.process(engine, wb)
print "upgrade"

ingr.process(engine, wb)
print "ingr"

pack.process(engine, wb)
print "pack"

pack_burger.process(engine, wb)
print "pb"

pack_side.process(engine, wb)
print "ps"

burger_contains.process(engine, wb)
print "bc"
