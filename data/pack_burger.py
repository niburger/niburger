import sys
sys.path.insert(0, '../flask')

from adminmanager import AdminManager
from helper import table2dict

def process(engine, wb):
    pb = wb['pack_burger']
    res = table2dict(16, 3, pb)

    adm = AdminManager(engine)
    for r in res:
        adm.add_burger_into_pack(r['id_pack'], r['id_burger'], r['cardinality'])
