import sys
sys.path.insert(0, '../flask')

from adminmanager import AdminManager
from helper import table2dict

def process(engine, wb):
    burger = wb['burger']
    res = table2dict(81, 12, burger)

    engine.execute("alter table Burger auto_increment = 1")
    adm = AdminManager(engine)
    for r in res:
        r['ingredients'] = []
        adm.add_burger(r)
