import sys
sys.path.insert(0, '..')

from sqlalchemy import create_engine
from dbmanager import DBManager
from adminmanager import AdminManager

engine = create_engine("mysql://root:ab1616@localhost/test")

dbm = DBManager(engine)
adm = AdminManager(engine)

print adm.get_brands()
print adm.get_ingrs()
brand = adm.get_brands()[0]['id_brand']
ingrs = map(lambda v: v['id_ingr'], adm.get_ingrs())

engine.execute("delete from Burger")
engine.execute("delete from Burger_contains")

burger = {
        'ea_name': 'chick',
        'ea_dsc': '',
        'ea_price': 1000,
        'ea_img': '',
        'set_name': '',
        'set_dsc': '',
        'set_price': 1000,
        'set_img': '',
        'has_lunch': False,
        'lunch_price': 0,
        'id_brand': brand,
        'ingredients': ingrs
        }

adm.add_burger(burger)
target = adm.search_burger('chick', 'lol')[0]['id_burger']
print target

burger['id_burger'] = target
burger['ea_dsc'] = 'delicious!'
adm.edit_burger(burger)

print adm.search_burger('chick', 'lol')

adm.delete_ingr_from_burger(target, adm.search_ingr('', 'tomato')[0]['id_ingr'])

print adm.search_burger('chick', 'lol')

adm.add_ingr_to_burger(target, adm.search_ingr('', 'tomato')[0]['id_ingr'])
adm.delete_ingr(adm.search_ingr('', 'tomato')[0]['id_ingr'])

print adm.search_burger('chick', 'lol')
print adm.get_ingrs_from_burger(adm.search_burger('chick', 'lol')[0]['id_burger'])

print 'Test of Gp 3 Done!'
