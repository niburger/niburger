import sys
sys.path.insert(0, '../flask')

from adminmanager import AdminManager
from helper import table2dict

def process(engine, wb):
    brand = wb['brand']
    res = table2dict(6, 5, brand)

    engine.execute("alter table Brand auto_increment = 1")
    adm = AdminManager(engine)
    for r in res:
        adm.add_brand(r)
