# -*- coding: utf-8 -*-

import random
from sets import Set
from sqlalchemy import text

class DBManager:
    def __init__(self, engine):
        self.engine = engine

    def res2dic(self, resultProxy):
        rows = resultProxy.fetchall()
        return map(lambda r: dict(zip(r.keys(), r.values())), rows)

    def getBrandNames(self):
        cmd = "select id_brand, name, contact, homepage " \
                "from Brand"
        return self.res2dic(self.engine.execute(cmd))

    def getIngrs(self):
        cmd = "select id_ingr, type, name, dsc, img " \
                "from Ingredient"
        return self.res2dic(self.engine.execute(cmd))

    def getUpgrades(self, brands):
        cmd = "select type, id_upgrade, name, dsc, price, img, id_brand " \
                "from Upgrade natural join Side " \
                "where id_brand = :brand"
        upgrades = sum(
                map(lambda brand:
                    self.res2dic(self.engine.execute(text(cmd), {'brand' : brand})),
                    brands),
                [])

        result_dict = {}
        for upgrade in upgrades:
            if upgrade['type'] in result_dict:
                result_dict[upgrade['type']].append(upgrade)
            else:
                result_dict[upgrade['type']] = [upgrade]

        ret = []
        for key, value in result_dict:
            ret.append({'type': key, 'sets': value})

        return ret

    def getMenus(self, brands, seed=False):
        cmd = "select id_burger, ea_name, ea_price, ea_img, id_brand " \
                "from Burger " \
                "where id_brand = :brand"
        burgers = sum(
                map(lambda brand:
                    self.res2dic(self.engine.execute(text(cmd), {'brand' : brand})),
                    brands),
                [])
        cmd = "select id_pack, name, price, img, id_brand " \
                "from Pack " \
                "where id_brand = :brand"
        packs = sum(
                map(lambda brand:
                    self.res2dic(self.engine.execute(text(cmd), {'brand' : brand})),
                    brands),
                [])
        cmd = "select id_side, name, price, img, id_brand " \
                "from Side " \
                "where id_brand = :brand"
        sides = sum(
                map(lambda brand:
                    self.res2dic(self.engine.execute(text(cmd), {'brand' : brand})),
                    brands),
                [])

        if seed:
            burgers = random.shuffle(burgers)
            packs = random.shuffle(packs)
        return {
                'burgers' : burgers,
                'packs' : packs
                }

    def getMenusByIngrs(self, brands, ingrs, seed=False): # Can I optimize it?
        l = len(ingrs)
        def subftn(brand):
            burgers = Set([])
            cmd = "select id_burger " \
                    "from burger " \
                    "where (id_brand = :brand)"
            res = self.engine.execute(text(cmd), {'brand' : brand})
            for row in res:
                burgers.add(row['id_burger']) # burgers: all burger of such brand

            for ingr in ingrs:
                here = Set([])
                cmd = "select id_burger " \
                        "from burger_contains " \
                        "where (id_brand = :brand) and " \
                        "(id_ingr = :ingr)"
                res = self.engine.execute(text(cmd), {'brand' : brand, 'ingr' : ingr})
                for row in res:
                    here.add(row['id_burger'])
                burgers &= here

            return burgers

        burger_set = reduce(lambda s,t: s.union(t), map(subftn, brands), Set([]))
        ret = []
        for burger in burger_set:
            cmd = "select count(id_burger) " \
                    "from burger_contains " \
                    "where (id_burger = :burger)"
            size = self.engine.execute(text(cmd), {'burger' : burger}).fetchone()[0]

            cmd = "select id_burger, ea_name, ea_price, ea_img " \
                    "from burger " \
                    "where (id_burger = :burger)"
            row = self.engine.execute(text(cmd), {'burger' : burger}).fetchone()
            elm = dict(zip(row.keys(), row.values()))
            elm['distance'] = size - l
            ret.append(elm)

        if seed:
            ret = random.shuffle(ret)
        else:
            ret = sorted(ret, lambda x,y: x['distance'] < y['distance'])

        return ret

    def getBurgerInfo(self, burger):
        cmd = "select id_burger, ea_name, ea_dsc, ea_price, ea_img, " \
                "set_name, set_dsc, set_price, set_img, " \
                "has_lunch, lunch_price, id_brand " \
                "from burger " \
                "where id_burger = :burger"
        return self.res2dic(self.engine.execute(text(cmd), {'burger' : burger}))[0]

    def getPackInfo(self, pack):
        cmd = "select id_pack, name, price, dsc, img, id_brand " \
                "from pack " \
                "where id_pack = :pack"
        return self.res2dic(self.engine.execute(text(cmd), {'pack' : pack}))[0]

    def smartSet(self, burgers, sides):
        # 각 side에 대응되는 upgrade들을 우선 fetch한다.
        cmd = "select id_upgrade, id_side, type " \
                "from Upgrade natrual join Side"  \
                "where id_side = :side"
        upgrades = Set(
                sum(
                    map(lambda side:
                        self.res2dic(self.engine.execute(text(cmd), {'side' : side})),
                        sides),
                    [])
                )

        cmd = "select (set_price - ea_price) as diff, id_burger " \
                "from Burger " \
                "where set_price is not null and " \
                "id_burger = :burger"
        diffs = sum(
                map(lambda burger:
                    self.res2dic(self.engine.execute(text(cmd), {'burger' : burger})),
                    burgers),
                [])

        diffs = sorted(diffs, lambda x,y: x['diff'] < y['diff'])

        # 각 upgrade들을 type 별로 분류한다.
        upgrades_by_type = {}
        for u in upgrades:
            if u['type'] in upgrades_by_type:
                upgrades_by_type[u['type']].add(u)
            else:
                upgrades_by_type[u['type']] = Set([u])

        res = []
        for burger in diffs:
            if not upgrades_by_type:
                break
            else:
                here = dict(id_burger = burger['id_burger'], id_sides = [], result = [])
                for _, ups in upgrades_by_type:
                    if ups:
                        u = ups.pop()
                        here['id_sides'].append(u['id_side'])
                        here['results'].append(u['id_upgrade'])
                if here['id_sides']:
                    res.append(here)

        return res
