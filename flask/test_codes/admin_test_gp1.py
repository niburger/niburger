import sys
sys.path.insert(0, '..')

from sqlalchemy import create_engine
from dbmanager import DBManager
from adminmanager import AdminManager

engine = create_engine("mysql://root:ab1616@localhost/test")
engine.execute("delete from Brand")

dbm = DBManager(engine)
adm = AdminManager(engine)

brand1 = {
        'name': 'lol',
        'contact': '123',
        'homepage': 'a.com',
        'img': 'test.jpg'
        }

brand2 = {
        'name': 'hi',
        'contact': '999',
        'homepage': 'naver.com',
        'img': 'naver.png'
        }

if not adm.add_brand(brand2):
    exit(1)
if not adm.add_brand(brand1):
    exit(1)

print adm.get_brands()
print adm.search_brand('o')

target = adm.get_brands()[0]['id_brand']

newbrand = {
        'name': 'shit',
        'contact': '45',
        'homepage': 't.net',
        'img': 'ttt.jpg',
        'id_brand': target
        }

if not adm.edit_brand(newbrand):
    exit(1)

print adm.get_brands()
adm.delete_brand(target)
print adm.get_brands()

print "Test of Gp 1 Done!"

exit(0)
