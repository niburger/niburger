import sys
sys.path.insert(0, '../flask')

from adminmanager import AdminManager
from helper import table2dict

def process(engine, wb):
    upgrade = wb['upgrade']
    res = table2dict(13, 7, upgrade)

    engine.execute("alter table Upgrade auto_increment = 1")
    adm = AdminManager(engine)
    for r in res:
        adm.add_upgrade(r)
